/*****************************************************************************/
/* Header Files                                                              */
/*****************************************************************************/

#include "timer1_module.h"
#include "pin_definitions.h"
#include <xc.h>

/*****************************************************************************/
/* Macros                                                                    */
/*****************************************************************************/

// Processor frequency.
#define F_P 40000000

/*****************************************************************************/
/* Global Variables                                                          */
/*****************************************************************************/

static volatile uint16_t led_timer = 0;

/*****************************************************************************/
/* Function Definitions                                                      */
/*****************************************************************************/

// Configure Timer1 to generate an interrupt every millisecond. Timer1 is used
// to blink an LED once per second.
void Timer1_init(void) {
    T1CONbits.TON = 0; // stop timer
    T1CONbits.TCS = 0; // select system peripheral clock
    T1CONbits.TGATE = 0; // gated time accumulation disabled
    T1CONbits.TCKPS = 0b01; // prescale by 8, so now 200ns clock
    PR1 = F_P / 8 / 1000 - 1; // 1000Hz interrupt (F_P / prescale / 1000hz - 1)
    IFS0bits.T1IF = 0; // clear interrupt flag
    IEC0bits.T1IE = 1; // enable interrupt
    T1CONbits.TON = 1; // start timer
}

/*****************************************************************************/
/* Interrupt Service Routines                                                */
/*****************************************************************************/

void __attribute__((__interrupt__, no_auto_psv)) _T1Interrupt(void) {
    // flip LED state if led_timer reaches zero
    led_timer = (led_timer + 1) % 1000; // 1000 ms, or 1 second
    if(!led_timer) {
        LED_PIN = ~LED_PIN;
    }

    // Clear Timer1 interrupt
    IFS0bits.T1IF = 0;
}
