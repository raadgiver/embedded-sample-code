/*****************************************************************************/
/* Header Files                                                              */
/*****************************************************************************/

#include "uart_module.h"
#include <xc.h>
#include "pin_definitions.h"
#include <string.h>
#include <stdbool.h>

/*****************************************************************************/
/* Macros                                                                    */
/*****************************************************************************/

#define F_P 40000000
#define BAUD_RATE 9600

// Note, if powers of 2 used for buffer size, it's possible to use bitwise AND
// operations instead of modulo operations.
#define RX_BUFFER_SIZE 30
#define TX_BUFFER_SIZE 60

/*****************************************************************************/
/* User-Defined Data Types                                                   */
/*****************************************************************************/

// Note, the code does not do error handling if the leading pointer ever
// catches up to the trailing pointer. This is not a fatal problem, but
// information will be thrown away.
struct RXRingBuffer {
    uint8_t trailing_ptr;
    uint8_t leading_ptr;
    char data[RX_BUFFER_SIZE];
};

struct TXRingBuffer {
    uint8_t trailing_ptr;
    uint8_t leading_ptr;
    char data[TX_BUFFER_SIZE];
};

/*****************************************************************************/
/* Global Variables                                                          */
/*****************************************************************************/

static volatile struct RXRingBuffer RX_queue = {
    0,
    0,
    "\0",
};

static volatile struct TXRingBuffer TX_queue = {
    0,
    0,
    "\0",
};

/*****************************************************************************/
/* Internal Function Prototypes                                              */
/*****************************************************************************/

/**
 * Indicates whether or not there is a new character received by the UART
 * module.
 * @return True if a new character is available, false if there is no new
 * character.
 */
bool UART_char_avail(void);

/**
 * Returns the oldest character received by the UART module and then shifts it
 * out of the buffer.
 * @return Returns a character from the RX buffer.
 */
char UART_get(void);

/*****************************************************************************/
/* Function Definitions                                                      */
/*****************************************************************************/

void UART_init(void) {
    U1MODEbits.UARTEN = 0; //turn off, reset FIFO buffer pointers and counters

    U1MODEHbits.BCLKSEL = 0b00; // select F_P as clock source
    U1MODEHbits.BCLKMOD = 1; // fractional Baud Rate Generation
    //U1BRG = F_P / BAUD_RATE = 40M/9.6k;
    U1BRG = 4168; // select baud rate
    U1MODEbits.MOD = 0b0000; // asynchronous 8-bit UART
    U1MODEHbits.UTXINV = 0; // standard polarity
    U1MODEHbits.STSEL = 0b00; // 1 stop bit
    U1MODEHbits.FLO = 0b00; // flow control off
    U1STAHbits.UTXISEL = 0b000; // TX interrupt triggered when there are 8
    // empty buffer slots. If greater "fluidity" of UART data transmission is
    // desired, tweak this register.
    U1STAHbits.URXISEL = 0b000; // RX interrupt triggered when there is 1
    // character or more in buffer

    U1MODEbits.UARTEN = 1; // ready to transmit/receive
    U1MODEbits.UTXEN = 1; // transmit enabled
    U1MODEbits.URXEN = 1; // receive enabled
    IFS0bits.U1TXIF = 0;
    IEC0bits.U1TXIE = 1;
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1; // interrupts enabled
}


char * UART_get_str(void) {
    // return value 1: empty string
    static char null_cmd[] = "";

    // return value 2: previous string
    static char previous_cmd[RX_BUFFER_SIZE] = "";

    // return value 3: current string
    static char current_cmd[RX_BUFFER_SIZE] = "";
    static uint8_t i = 0;

    if(UART_char_avail()) {
        char recvd_char = UART_get();
        switch(recvd_char) {
            case '\r':
                if(i == 0) {
                    return previous_cmd;
                } else {
                    current_cmd[i] = '\0';
                    strcpy(previous_cmd, current_cmd);
                    return current_cmd;
                }
                break;
            case '\n':
                break;
            case '\b':
                if(i != 0) {
                    --i;
                }
                break;
            default:
                current_cmd[i] = recvd_char;
                UART_send_char(recvd_char); //echo to user
                i = (i + 1) % RX_BUFFER_SIZE;
        }
    }
    return null_cmd;
}


bool UART_char_avail(void) {
    if(RX_queue.trailing_ptr != RX_queue.leading_ptr) {
        return true;
    } else {
        return false;
    }
}


// Note, if a new character isn't available, this function will return the most
// recently received character instead (or \0 if no character has been received
// yet).
char UART_get(void) {
    char uart_char = RX_queue.data[RX_queue.trailing_ptr];

    if(RX_queue.trailing_ptr != RX_queue.leading_ptr) {
        RX_queue.trailing_ptr = (RX_queue.trailing_ptr + 1) % RX_BUFFER_SIZE;
    }

    return uart_char;
}


void UART_send_char(char my_char) {
    // Put character in TX ring buffer.
    TX_queue.data[TX_queue.leading_ptr] = my_char;
    TX_queue.leading_ptr = (TX_queue.leading_ptr + 1) % TX_BUFFER_SIZE;

    // Call U1TXInterrupt like a function.
    IFS0bits.U1TXIF = 1;
}


void UART_send_str(char * mystring) {
    uint8_t mystring_len = strlen(mystring);
    uint8_t i;

    // Put string in TX ring buffer.
    for(i = 0; i < mystring_len; i++) {
        TX_queue.data[TX_queue.leading_ptr] = mystring[i];
        TX_queue.leading_ptr = (TX_queue.leading_ptr + 1) % TX_BUFFER_SIZE;
    }

    // Call U1TXInterrupt like a function.
    IFS0bits.U1TXIF = 1;
}

/*****************************************************************************/
/* Interrupt Service Routines                                                */
/*****************************************************************************/

void __attribute__((__interrupt__, no_auto_psv)) _U1RXInterrupt(void) {
    // Receive characters while UART RX buffer isn't empty.
    while(!U1STAHbits.URXBE) {
        char recvd_char = U1RXREG;
        RX_queue.data[RX_queue.leading_ptr] = recvd_char;
        RX_queue.leading_ptr = (RX_queue.leading_ptr + 1) % RX_BUFFER_SIZE;

        // Echo character back to user. Note, if U1RXInterrupt occurs while
        // TX_queue is being written to elsewhere, one of the values already in
        // TX_queue will be overwritten.
        // TODO Make a separate mini-buffer for characters that need to be
        // echoed back to the user.
        UART_send_char(recvd_char);
    }

    // TODO If buffer overflow occurs, fix here.
    if(U1STAbits.OERR) {
        U1STAbits.OERR = 0;
    }

    // clear interrupt flag
    IFS0bits.U1RXIF = 0;
}


void __attribute__((__interrupt__, no_auto_psv)) _U1TXInterrupt(void) {
    // Send characters while UART TX buffer isn't full and there is information
    // available to send.
    while(!U1STAHbits.UTXBF && (TX_queue.trailing_ptr != TX_queue.leading_ptr)) {
        U1TXREG = TX_queue.data[TX_queue.trailing_ptr];
        TX_queue.trailing_ptr = (TX_queue.trailing_ptr + 1) % TX_BUFFER_SIZE;
    }

    // TODO If buffer overflow occurs, fix here.
    if(U1STAHbits.TXWRE) {
        U1STAHbits.TXWRE = 0;
    }

    // clear interrupt flag
    IFS0bits.U1TXIF = 0;
}
