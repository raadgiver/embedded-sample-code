// Author: Robert Noble
// Board(s): WEP 4002021 REV A

// This project is intended to be used as a starting point for code for the
// dsPIC33CK256MP508 microprocessor, though it's probably flexible enough to be
// used by other Microchip processors (with some minor tweaks). It can also be
// used for testing simple code.
// By convention, code that interacts directly with hardware modules goes into
// its own file.

/*****************************************************************************/
/* Header Files                                                              */
/*****************************************************************************/

#include "sysinit.h"
#include <xc.h>
#include "uart_module.h"
#include <string.h>

/*****************************************************************************/
/* Processor Configuration                                                   */
/*****************************************************************************/

// Oscillator Source Selection (Internal Fast RC (FRC))
#pragma config FNOSC = FRC
#pragma config IESO = OFF

// Enable Clock Switching and Configure POSC in XT mode
#pragma config POSCMD = XT 
#pragma config FCKSM = CSECMD

//when power on, turn off watchdog
#pragma config FWDTEN = ON_SW

//sample board use PGD3/PGC3 
#pragma config JTAGEN = OFF
#pragma config ICS = PGD3

/*****************************************************************************/
/* Main Loop                                                                 */
/*****************************************************************************/

int main(void) {
    sys_init();
    UART_send_str("\r\nSystem start\r\n");

    while(1) {
        char * blah = UART_get_str();
        if(strcmp(blah, "")) {
            UART_send_str("\r\nInput received: ");
            UART_send_str(blah);
        }
    }

    return 0;
}
