#ifndef SYSINIT_H
#define	SYSINIT_H

/*****************************************************************************/
/* Function Prototypes                                                       */
/*****************************************************************************/

/**
 * Initializes the crystal oscillator, UART module, Timer1 module, and IO
 * ports.
 */
void sys_init(void);

#endif

