#ifndef TIMER1_MODULE_H
#define	TIMER1_MODULE_H

/*****************************************************************************/
/* Function Prototypes                                                       */
/*****************************************************************************/

/**
 * Initializes the Timer1 module to generate an interrupt every 1000Hz (1ms).
 * This interrupt flips the state of an LED every 1 second.
 */
void Timer1_init(void);

#endif

