#ifndef UART_MODULE_H
#define	UART_MODULE_H

/*****************************************************************************/
/* Header Files                                                              */
/*****************************************************************************/



/*****************************************************************************/
/* Function Prototypes                                                       */
/*****************************************************************************/

/**
 * Initializes UART module with 9.6 kbaud, 8-bit data, 1 stop bit, no parity.
 */
void UART_init(void);

/**
 * Checks the UART RX buffer and returns different values depending on what the
 * RX buffer contains.
 * 1. If the carriage return character (CR) has not been received by the UART,
 * this function returns a pointer to an empty string.
 * 2. If CR has been received and is the first character, this function returns
 * a pointer to the previous UART input string.
 * 3. If CR has been received and is not the first character, this function
 * returns a pointer to the freshly completed input string.
 */
char * UART_get_str(void);

/**
 * Copies a single character into the UART TX buffer and manually calls the
 * UART TX interrupt.
 */
void UART_send_char(char);

/**
 * Copies a single string into the UART TX buffer and manually calls the UART
 * TX interrupt.
 */
void UART_send_str(char *);


#endif
