/*****************************************************************************/
/* Header Files                                                              */
/*****************************************************************************/

#include "sysinit.h"
#include "timer1_module.h"
#include "uart_module.h"
#include <xc.h>
#include "pin_definitions.h"

/*****************************************************************************/
/* Internal Function Prototypes                                              */
/*****************************************************************************/

/**
 * Switches from the FRC oscillator to the crystal oscillator and sets the
 * instruction clock speed to 40MHz.
 */
static void oscillator_init(void);

/**
 * Configures processor pins for UART TX/RX and LED blinking.
 */
static void port_init(void);

/*****************************************************************************/
/* Function Definitions                                                      */
/*****************************************************************************/

void sys_init(void) {
    oscillator_init();
    UART_init();
    Timer1_init();
    port_init();
}


static void oscillator_init(void) {
    // For a desired instruction clock speed of F_P = 40MHz:
    // F_OSC = F_P * 2 = 80 (MHz)
    // F_PLLO = F_OSC * 2 = 160
    // Assuming a F_PLLI = 16MHz crystal and using equations in the manual:
    // F_PLLO/F_PLLI = 10 = M/(N1*N2*N3)
    // with the constraints:
    // 400 < F_VCO=F_PLLI*M/N1 < 1600
    // 8 < F_PFD=F_PLLI/N1 < F_VCO/16
    CLKDIVbits.PLLPRE = 2; // N1 = 2
    PLLFBDbits.PLLFBDIV = 100; // M = 100 
    PLLDIVbits.POST1DIV = 5; // N2 = 5
    PLLDIVbits.POST2DIV = 1; // N3 = 1

    // Initiate Clock Switch to Primary Oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONH(0b011);
    __builtin_write_OSCCONL(OSCCON | 0x01);
    // Wait for Clock switch to occur
    while(OSCCONbits.OSWEN != 0);
    // Wait for PLL to lock
    while(OSCCONbits.LOCK != 1);
}


static void port_init(void) {
    // set all pins to digital I/O
    // necessary since all ANSEL registers are set on system boot
    ANSELA = ANSELB = ANSELC = ANSELD = ANSELE = 0;

    // set all pins to be output pins
    // necessary since all TRIS registers are set on system boot
    TRISA = TRISB = TRISC = TRISD = TRISE = 0;
    TRISDbits.TRISD3 = 1; // Set UART U1RX as input port.

    // enable peripheral remapping
    __builtin_write_RPCON(0x0000);
    {
        // UART1 RX to RP67 (pin 69)
        RPINR18bits.U1RXR = 67;

        // UART1 TX to RP68 (pin 68)
        RPOR18bits.RP68R = 1;
    }
    // disable peripheral remapping
    __builtin_write_RPCON(0x0800);

    // set LED pin to 1, to see if the processor is alive
    LED_PIN = 1;
}
